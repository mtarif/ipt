#!/usr/bin/env python3
import os # for terminal session
import sys # for arguments
import getpass # to check is root or no

args = sys.argv # get arguments
output = 'apt' # output will run in terminal
help = " -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n \
| ipt operations package1 package2 ... flags |\n \
-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n \
Supported operations:\n \
  u  -> update\n \
  uu -> update and upgrade\n \
  fu -> update and full-upgrade\n \
  i  -> install\n \
  r  -> remove\n \
  R  -> autoremove\n \
  p  -> purge\n \
  S  -> search\n \
  s  -> show\n \
  l  -> list" #help comment

if len(args) == 1: #check have argument
	output = 0
	print(help)
elif args[1] in ['S', 's', 'l', '--help', '-help', 'help', '-h', '--h']: # search, show and list don't need root
	if args[1] in ['--help', '-help', 'help', '-h', '--h']:
		output = 0
		print(help)
	elif args[1] == 'S': #search
		output+= ' search '
		output+= ' '.join(args[2:]) # packs to string
	elif args[1] == 's': #show
		output+= ' show '
		output+= ' '.join(args[2:])
	elif args[1] == 'l': #list, can have this flags: -a (all), -i (installed), -u (upgradeble)
		output+= ' list '
		output+= ' '.join(args[2:])
elif getpass.getuser() == 'root': #check user run this script is root
	flags = ['-y', '-d', '-f', \
			 '-yd', '-dy', '-yf', '-fy', '-df', '-fd', \
			 '-ydf', '-yfd', '-dyf', '-dfy', '-fyd', '-fdy'] #3 flags most used, it's important the place of use
	tails = [i for i in flags if i in args] #select all flags are every place of query and save to add last
	[i for i in tails if args.remove(i)] # remove all flags, because we have them in tails and don't need them

	if args[1] == 'u':
		output+= ' update '
	elif args[1] == 'uu':
		output+= ' update && apt upgrade '
	elif args[1] == 'fu':
		output+= ' update && apt full-upgrade '
	elif args[1] == 'i':
		output+= ' install '
		output+= ' '.join(args[2:])
	elif args[1] == 'ui':
		output+= ' update && apt install '
		output+= ' '.join(args[2:])
	elif args[1] == 'uui':
		output+= ' update && apt upgrade && apt install '
		output+= ' '.join(args[2:])
	elif args[1] == 'fui':
		output+= ' update && apt full-upgrade && apt install '
		output+= ' '.join(args[2:])
	elif args[1] == 'r':
		output+= ' remove '
		output+= ' '.join(args[2:])
	elif args[1] == 'R':
		output+= ' autoremove '
		output+= ' '.join(args[2:])
	elif args[1] in ['p', 'rp', 'pr']:
		output+= ' purge '
		output+= ' '.join(args[2:])
	elif args[1] in ['Rp', 'pR']:
		output+= ' autoremove --purge '
		output+= ' '.join(args[2:])
	elif args[1] == 'c':
		output+= ' clean '
	elif args[1] == 'C':
		output+= ' autoclean '
	else:
		output = None

	if output: # if not none
		sflags = ['-y', '-d', '-yd', '-dy'] #special flags
		fsflags = [i for i in sflags if i in tails] #founded special flags
		if fsflags and args[1] in ['uu', 'uui', 'fu', 'fui']: #if founded true
			strflags = ''
			strflags+= ' '.join(fsflags) #founded special flags in str
			output = output.replace('install', 'install ' + strflags) # add to install
			output = output.replace('upgrade', 'upgrade ' + strflags) # add to upgrade
			output = output.replace('full-upgrade', 'full-upgrade ' + strflags) # add to full-upgrade
		else: #if not special flags, add them to end
			output+= ' ' + ' '.join(tails)
else: #not root? show good msg
	output = 0
	print(" Permission denied, are you root?:)")

if output: # if added query to output,
	os.system(output) # run it!
elif output == None:
	print(" Error: Invalid operation")
	print(help)
