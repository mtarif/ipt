#!/usr/bin/env python3
import os, getpass, shutil

ipt = '/usr/bin/ipt'
iptfile = os.getcwd() + '/ipt.py'

print("Installing ...")

if getpass.getuser() == 'root':
	if os.path.isfile(ipt):
		os.remove(ipt)
	shutil.copyfile(iptfile, ipt)
	os.chmod(ipt, 493)
	print(" ipt installed succefully. for help, type:\n ipt help")
else:
	print(" Permission denied, are you root?:)")