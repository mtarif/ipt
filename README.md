# ipt - Improved advanced Package Manager for debian-based distributions

`ipt` is an **apt base package manager** that simplifies tasks. For example, have you ever wanted to update the list of repositories, upgrade the system and install some packages? You might enter the following code:

<pre>sudo apt update && sudo apt upgrade -y && sudo apt install xfce4 xfce4-goodies -y</pre>

But writing such a code is very difficult, `ipt` shows you a better way:

<pre>sudo ipt uui xfce4 xfce4-goodies -y</pre>

**that's it!**

## Installtion

First clone the project:

<pre>git clone https://gitlab.com/mtarif/ipt.git</pre>

Change to project directory, give permission to `install.sh` and run if as `root`:

<pre>cd ipt && chmod 755 install.sh && sudo ./install.sh</pre>

## Tutorial

The structure of the commands is as follows:

<pre>ipt operations package1 package2 ... flags</pre>

**Examples:**

*   update and upgrade a special package:

    <pre>sudo ipt uu xfce4</pre>

*   update and full-upgrade a special package:

    <pre>sudo ipt fu xfce4</pre>
    
**Operations list**

<table>

<thead>

<tr>
<th>operation</th>
<th>Converted to</th>
</tr>

</thead>

<tbody>

<tr>
<td>u</td>
<td>update</td>
</tr>

<tr>
<td>uu</td>
<td>update & upgrade</td>
</tr>

<tr>
<td>fu</td>
<td>update & full-upgrade</td>
</tr>

<tr>
<td>i</td>
<td>install</td>
</tr>

<tr>
<td>r</td>
<td>remove</td>
</tr>

<tr>
<td>R</td>
<td>autoremove</td>
</tr>

<tr>
<td>p</td>
<td>purge</td>
</tr>

<tr>
<td>S</td>
<td>search</td>
</tr>

<tr>
<td>s</td>
<td>show</td>
</tr>

<tr>
<td>l</td>
<td>list</td>
</tr>

</tbody>

</table>

You can combine `operations` to do **more**, like:

*   update and install a package:

    <pre>sudo ipt ui xfce4</pre>

*   update and upgrade and install a package:

    <pre>sudo ipt uui xfce4</pre>

*   update and full-upgrade and install a package:

    <pre>sudo ipt fui xfce4</pre>

*   remove and purge a package:

    <pre>sudo ipt rp xfce4</pre>

*   autoremove (remove package and dependencies) and purge a package:

    <pre>sudo ipt Rp xfce4</pre>
    
**Flags**

Use flags to do more, some examples:

*	Just write once `-y`, `ipt` converts to `upgrade -y` and `install -y`!

	<pre>sudo ipt uui xfce -y</pre>

*	To fix broken packages, use `i` operation `-f`:

	<pre>sudo ipt i -f</pre>

*	Some `l` (list operation) useful flags:

	<pre>ipt l -i # list installed packages
    ipt l -u # list upgradable packages
    ipt l -a # list all packages in repository</pre>